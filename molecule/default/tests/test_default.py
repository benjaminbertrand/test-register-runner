import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('gitlab_runner')


def test_molecule_envs(host):
    assert host.file('/opt/conda/envs/molecule/bin/molecule').exists
    assert host.file('/opt/conda/envs/molecule2/bin/molecule').exists


def test_packer_installed(host):
    cmd = host.run('packer --version')
    assert cmd.rc == 0


def test_vagrant_installed(host):
    cmd = host.run('vagrant version')
    assert cmd.rc == 0


def test_docker_installed(host):
    docker = host.service('docker')
    assert docker.is_running
    assert docker.is_enabled
