ics-ans-role-gitlab-runner
==========================

Ansible role to install gitlab-runner.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
# Set to latest or the exact version number
gitlab_runner_version: latest
# List of runners to register
# gitlab_runner_to_register:
#   - name: myrunner1
#     tags: tag1,tag2
#     executor: docker
#     extra_options: "--docker-volumes /var/run/docker.sock:/var/run/docker.sock"
#   - name: myrunner2
#     tags: tag3
#     executor: shell
gitlab_runner_to_register: []
gitlab_runner_registration_token: token
gitlab_runner_url: https://mygitlab.example.org
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-gitlab-runner
```

License
-------

BSD 2-clause
